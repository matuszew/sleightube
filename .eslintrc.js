const path = require('path');

module.exports = {
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "node": true,
  },
  "extends": ["eslint:recommended", "airbnb", "react-app"],
  "rules": {
    "import/prefer-default-export": "off",
    "react/jsx-filename-extension": "off",
    "react/jsx-sort-props": ["error", {"ignoreCase": true}],
    "react/sort-prop-types": ["error", {"ignoreCase": true}],
    "jsx-a11y/href-no-hash": "off",
    "jsx-a11y/click-events-have-key-events": "off",
    "jsx-a11y/anchor-is-valid": ["warn", { "aspects": ["invalidHref"] }],
    "max-len": ["error", 120],
    "no-unused-vars": "error",
    "no-console": "error",
    "no-undef": 'off', // FIXME wait for fix related to optional chaining plugin
    "object-curly-newline": "off",
    "wyze/sort-destructuring-keys": "error",
  },
  "globals": {
    "NODE_ENV": true
  },
  "settings": {
    "import/resolver": {
      "webpack": {
        "config": path.resolve(__dirname, 'config/webpack/webpack.config.dev.js')
      }
    }
  },
  "plugins": [
    "wyze"
  ]
};
