## Requirements

* node
* yarn

# Local run

Create `.env` file in root of the project with following values 

( Preview .env.example ) file

```
NODE_ENV=development
REACT_APP_YOUTUBE_API_KEY=AIzaSyA10xeZ7ucELsxDBOT_kXyR8ts3AggabxY
REACT_APP_YOUTUBE_CLIENT_ID=183351144238-b4ls9avf82r0ssj26366b9j8s7l6umgt.apps.googleusercontent.com
REACT_APP_YOUTUBE_SCOPE=profile https://www.googleapis.com/auth/youtube
```

## How to run

1. yarn install
2. yarn start

