process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

const webpack = require('webpack');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackDevMiddleware = require('webpack-dev-middleware');
const express = require('express');
const compression = require('compression');
const webpackConfig = require('../config/webpack/webpack.config.dev');

const app = express();
const compiler = webpack(webpackConfig);

const {
  choosePort,
  prepareUrls
} = require('react-dev-utils/WebpackDevServerUtils');
const openBrowser = require('react-dev-utils/openBrowser');

const DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3000;
const HOST = process.env.HOST || '0.0.0.0';
const protocol = process.env.HTTPS === 'true' ? 'https' : 'http';

choosePort(HOST, DEFAULT_PORT)
  .then(port => {
    if (port === null) {
      return;
    }

    const urls = prepareUrls(protocol, HOST, port);

    app.use(compression({}));
    app.use(webpackDevMiddleware(compiler, {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath,
      stats: 'errorsOnly',
    }));
    app.use(webpackHotMiddleware(compiler));
    app.listen(port, () => {
      console.log(`Starting webpack server at ${urls.localUrlForTerminal}`);
      console.log(`Remote webpack server is accessible at ${urls.lanUrlForTerminal}`);
    });
    openBrowser(urls.localUrlForBrowser);
  });
