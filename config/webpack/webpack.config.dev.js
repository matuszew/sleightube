const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const path = require('path');
const paths = require('../paths');
const postCssFlex = require('postcss-flexbugs-fixes');
const Dotenv = require('dotenv-webpack');
const webpack = require('webpack');
const rxPaths = require('rxjs/_esm5/path-mapping');

const postCssOptions = {
  ident: 'postCss',
  plugins: () => [
    postCssFlex,
  ],
};

module.exports = {
  context: paths.appSrc,
  devtool: 'eval-source-map',
  entry: [
    require.resolve('../polyfills'),
    paths.appIndex,
    require.resolve('webpack-hot-middleware/client'),
  ],
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: paths.appSrc,
        use: [
          'babel-loader',
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true, // true outputs JSX tags
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              camelCase: 'dashes',
              localIdentName: '[local]--[hash:base64:5]',
              modules: true,
            },
          },
          {
            loader: 'url-tilde-loader',
            options: {
              replacement: '~/../',
            },
          },
          {
            loader: 'postcss-loader',
            options: postCssOptions,
          },
          {
            loader: 'sass-loader',
            options: {
              includePaths: [
                paths.appNodeModules,
              ],
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              camelCase: 'dashes',
              localIdentName: '[local]--[hash:base64:5]',
              modules: true,
            },
          },
          {
            loader: 'url-tilde-loader',
            options: {
              replacement: '~/../',
            },
          },
          {
            loader: 'postcss-loader',
            options: postCssOptions,
          },
        ],
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'static/js/bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: paths.appHtml,
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new Dotenv(),
  ],
  resolve: {
    alias: rxPaths(),
    modules: [
      'node_modules',
      paths.appNodeModules,
      paths.appSrc,
    ],
    extensions: ['.js', '.css', '.scss'],
    plugins: [
      new ModuleScopePlugin(paths.appSrc, [paths.appPackageJson]),
    ],
  },
};
