const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());
const resolvePath = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
  appHtml: resolvePath('public/index.html'),
  appIndex: resolvePath('src/index.js'),
  appNodeModules: resolvePath('node_modules'),
  appPackageJson: resolvePath('package.json'),
  appSrc: resolvePath('src'),
};
