import PropTypes from 'prop-types';

export const Movie = {
  id: PropTypes.string,
  channelTitle: PropTypes.string,
  description: PropTypes.string,
  title: PropTypes.string,
  thumbnail: PropTypes.string,
};

