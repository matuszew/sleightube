import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Spinner } from 'common/components/Indicator/Spinner/Spinner';

import { MoviePreview } from 'Homepage/MoviesPreview/MoviePreview/MoviePreview';

import { Movie as MoviePreviewSchema } from 'Homepage/schema';

import styles from './styles.scss';

export const MoviesPreview = ({
  searchResults,
  searchResultsLoading,
  selectMovie,
}) => (
  <Fragment>
    { searchResultsLoading
      ? <Spinner className={styles.MoviesPreview_Loader} />
      : searchResults.map(searchResult => (
        <MoviePreview
          key={searchResult.id}
          movie={searchResult}
          onClick={selectMovie}
        />
      ))
    }
  </Fragment>
);

MoviesPreview.propTypes = {
  searchResults: PropTypes.arrayOf(PropTypes.shape(MoviePreviewSchema)).isRequired,
  searchResultsLoading: PropTypes.bool.isRequired,
  selectMovie: PropTypes.func.isRequired,
};
