import { connect } from 'react-redux';

import { MoviesPreview as MoviesPreviewView } from 'Homepage/MoviesPreview/MoviesPreview';
import { isDataFetching } from 'common/store/repository/selectors';
import { NAMESPACE } from 'common/consts/namespaces';

import { getSearchResults } from './selectors';

const mapStateToProps = state => ({
  searchResults: getSearchResults(state),
  searchResultsLoading: isDataFetching(NAMESPACE.SEARCH)(state),
});

const mapDispatchToProps = dispatch => ({});

export const MoviesPreview = connect(mapStateToProps, mapDispatchToProps)(MoviesPreviewView);
