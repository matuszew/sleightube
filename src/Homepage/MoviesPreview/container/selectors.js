import { createSelector } from 'reselect';

import { getData } from 'common/store/repository/selectors';
import { NAMESPACE } from 'common/consts/namespaces';

export const getSearchResults = createSelector(
  getData(NAMESPACE.SEARCH, []),
  (data) => {
    if (!data?.items) {
      return [];
    }

    const searchResults = data.items.map(foundResult => ({
      id: foundResult.id?.videoId,
      channelTitle: foundResult.snippet?.channelTitle,
      description: foundResult.snippet?.description,
      title: foundResult.snippet?.title,
      thumbnail: foundResult.snippet?.thumbnails?.default?.url,
    }));

    return searchResults;
  },
);
