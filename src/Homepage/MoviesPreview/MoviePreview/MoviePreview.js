import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { Movie as MoviePreviewSchema } from 'Homepage/schema';
import { HeaderSmallBlock } from 'common/components/Typography/HeaderSmallBlock/HeaderSmallBlock';
import { TextParagraph } from 'common/components/Typography/TextParagraph/TextParagraph';

import styles from './styles.scss';

export const MoviePreview = ({
  className,
  movie,
  onClick,
}) => (
  <a
    className={styles.MoviePreview_Wrapper}
    key={movie.id}
    onClick={() => onClick(movie)}
    role="button"
    tabIndex="0"
  >
    <div className={classnames(styles.MoviePreview, className)}>
      <div className={styles.MoviePreview_ThumbnailContainer}>
        <img
          alt={movie.title}
          className={styles.MoviePreview_Thumbnail}
          src={movie.thumbnail}
        />
      </div>
      <div className={styles.MoviePreview_DetailsContainer}>
        <HeaderSmallBlock className={styles.MoviePreview_Header}>
          {movie.title}
        </HeaderSmallBlock>
        <TextParagraph className={styles.MoviePreview_Channel}>
          {movie.channelTitle}
        </TextParagraph>
      </div>
    </div>
  </a>
);

MoviePreview.propTypes = {
  className: PropTypes.string,
  movie: PropTypes.shape(MoviePreviewSchema).isRequired,
  onClick: PropTypes.func.isRequired,
};

MoviePreview.defaultProps = {
  className: '',
};
