import React, { Component, Fragment } from 'react';

import { MoviesPreview } from 'Homepage/MoviesPreview/container/connect';
import { MovieContent } from 'Homepage/MovieContent/container/connect';
import { MainContainer } from 'common/components/MainContainer/MainContainer';
import { Sidebar } from 'common/components/Sidebar/Sidebar';


export class Homepage extends Component {
  state = {
    selectedMovie: undefined,
  };

  selectMovie = (movie) => {
    this.setState({
      selectedMovie: movie,
    });
  };

  render() {
    const {
      selectedMovie,
    } = this.state;

    return (
      <Fragment>
        <MainContainer>
          <MovieContent movie={selectedMovie} />
        </MainContainer>
        <Sidebar>
          <MoviesPreview selectMovie={this.selectMovie} />
        </Sidebar>
      </Fragment>
    );
  }
}
