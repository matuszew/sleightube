import { connect } from 'react-redux';

import { isUserAuthenticated } from 'common/store/auth/selectors';

import { MovieContent as MovieContentView } from 'Homepage/MovieContent/MovieContent';

const mapStateToProps = state => ({
  isAuthenticated: isUserAuthenticated(state),
});

const mapDispatchToProps = dispatch => ({});

export const MovieContent = connect(mapStateToProps, mapDispatchToProps)(MovieContentView);
