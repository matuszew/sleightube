import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { HeaderLargeBlock } from 'common/components/Typography/HeaderLargeBlock/HeaderLargeBlock';
import { TextParagraph } from 'common/components/Typography/TextParagraph/TextParagraph';
import { Button } from 'common/components/Form/Button/Button';

import { Movie } from 'Homepage/schema';

import mainStyles from 'common/styles/main.scss';
import styles from './styles.scss';

const VIDEO_RATING = {
  LIKE: 'like',
  DISLIKE: 'dislike',
  NONE: 'none',
};

const movieContentButtonClasses = classnames(
  mainStyles.mdcButton,
  styles.MovieContent_ActionButton,
);

const movieContentButtonIconClasses = classnames(
  'material-icons',
  mainStyles.mdcButton__icon,
  styles.MovieContent_ActionIcon,
);

export class MovieContent extends Component {
  state = {
    liked: false,
    disliked: false,
  };

  likeVideo = () => {
    window.gapi.client.youtube.videos.rate({
      id: this.props.movie.id,
      rating: VIDEO_RATING.LIKE,
    }).then(() => {
      this.setState({ liked: true });
      this.setState({ disliked: false });
    });
  };

  dislikeVideo = () => {
    window.gapi.client.youtube.videos.rate({
      id: this.props.movie.id,
      rating: VIDEO_RATING.DISLIKE,
    }).then(() => {
      this.setState({ disliked: true });
      this.setState({ liked: false });
    });
  };

  render() {
    const {
      className,
      isAuthenticated,
      movie,
    } = this.props;

    if (!movie) {
      return <div />;
    }

    const likeClasses = classnames(
      movieContentButtonIconClasses,
      {
        [styles.MovieContent_ActionIcon__applied]: this.state.liked,
      },
    );

    const dislikeClasses = classnames(
      movieContentButtonIconClasses,
      {
        [styles.MovieContent_ActionIcon__applied]: this.state.disliked,
      },
    );

    return (
      <Fragment>
        <div className={classnames(styles.MovieContent, className)}>
          <iframe
            frameBorder="0"
            height="360"
            id="player"
            src={`http://www.youtube.com/embed/${movie?.id}?enablejsapi=1`}
            type="text/html"
            width="640"
          />
        </div>
        <div className={styles.MovieContent_Details}>
          <div className={styles.MovieContent_HeaderWrapper}>
            <HeaderLargeBlock className={styles.MovieContent_Title}>
              {movie.title}
            </HeaderLargeBlock>
            { isAuthenticated && (
              <div className={styles.MovieContent_Actions}>
                <Button
                  className={movieContentButtonClasses}
                  onClick={this.likeVideo}
                >
                  <i className={likeClasses}>
                    thumb_up
                  </i>
                </Button>
                <Button
                  className={movieContentButtonClasses}
                  onClick={this.dislikeVideo}
                >
                  <i className={dislikeClasses}>
                    thumb_down
                  </i>
                </Button>
              </div>
            )}
          </div>
          <TextParagraph>
            {movie.description}
          </TextParagraph>
        </div>
      </Fragment>
    );
  }
}

MovieContent.propTypes = {
  className: PropTypes.string,
  isAuthenticated: PropTypes.bool.isRequired,
  movie: PropTypes.shape(Movie),
};

MovieContent.defaultProps = {
  className: '',
  movie: null,
};
