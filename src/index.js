import React from 'react';
import ReactDOM from 'react-dom';
import { App } from 'App/App';
import { createHashHistory } from 'history';
import { createRootStore } from 'common/store/rootStore';
import { YoutubeScriptLoader } from 'common/utils/YoutubeScriptLoader';

import 'common/styles/main.scss';

const history = createHashHistory();

const store = createRootStore({}, history);

ReactDOM.render(
  <App history={history} store={store} />,
  document.getElementById('root'),
  () => {
    YoutubeScriptLoader();
  },
);
