import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { hot } from 'react-hot-loader';
import { ConnectedRouter } from 'react-router-redux';
import { Route, Switch } from 'react-router';

import { GoogleApiLoader } from 'common/utils/GoogleApiLoader';
import { ROUTER_CONFIG } from 'common/routing/routing';
import { Spinner } from '../common/components/Indicator/Spinner/Spinner';

import styles from './App.scss';

export class Application extends Component {
  state = {
    applicationLoading: true,
  };

  componentDidMount() {
    window.gapi.load('client:auth2', this.authModulesLoaded);
  }

  authModulesLoaded = () => {
    GoogleApiLoader().then(() => {
      this.setState({ applicationLoading: false });
    });
  };

  render() {
    const {
      history,
      store,
    } = this.props;

    if (this.state.applicationLoading) {
      return <div className={styles.AppLoader_Wrapper}><Spinner /></div>;
    }

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            {ROUTER_CONFIG.map(route => (
              <Route
                component={route.component}
                exact={route.exact}
                key={route.path}
                path={route.path}
              />
            ))
            }
          </Switch>
        </ConnectedRouter>
      </Provider>
    );
  }
}

Application.propTypes = {
  history: PropTypes.object.isRequired, // eslint-disable-line
  store: PropTypes.object.isRequired, // eslint-disable-line
};

export const App = hot(module)(Application);
