import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { APP_PATHS } from 'common/routing/path';

import { LogIn as LogInView } from 'LogIn/LogIn';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  redirectToHomePage: () => dispatch(push(APP_PATHS.HOMEPAGE)),
});

export const LogIn = connect(mapStateToProps, mapDispatchToProps)(LogInView);
