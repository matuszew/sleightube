import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import { MainContainer } from 'common/components/MainContainer/MainContainer';
import { HeaderLargeBlock } from 'common/components/Typography/HeaderLargeBlock/HeaderLargeBlock';
import { Button } from 'common/components/Form/Button/Button';
import { APP_PATHS } from 'common/routing/path';

import mainStyles from 'common/styles/main.scss';
import styles from './styles.scss';

const loginContainerActionClasses = classnames(
  styles.LoginContainer_Actions,
  mainStyles.mdcCard__action,
);

const loginContainerWrapperClasses = classnames(
  mainStyles.mdcCard,
  styles.LoginContainer_Wrapper,
);

const loginContainerActionButtonsClasses = classnames(
  mainStyles.mdcCard__actionButtons,
  styles.LoginContainer_ActionsButtons,
);

const loginActionButtonClasses = classnames(
  mainStyles.mdcButton,
  mainStyles.mdcCard__action,
  mainStyles.mdcCard__actionButton,
);

export class LogIn extends Component {
  signIn = () => {
    this.props.redirectToHomePage();
    window.gapi.auth2.getAuthInstance().signIn();
  };

  render() {
    return (
      <MainContainer className={styles.LoginContainer} fullWidth>
        <div className={loginContainerWrapperClasses}>
          <HeaderLargeBlock className={styles.LoginContainer_Title}>
            Login
          </HeaderLargeBlock>
          <div className={loginContainerActionClasses}>
            <div className={loginContainerActionButtonsClasses}>
              <Button
                className={loginActionButtonClasses}
                onClick={this.signIn}
              >
                Login
              </Button>
              <Link
                className={loginActionButtonClasses}
                to={APP_PATHS.HOMEPAGE}
              >
                View as Guest
              </Link>
            </div>
          </div>
        </div>
      </MainContainer>
    );
  }
}

LogIn.propTypes = {
  redirectToHomePage: PropTypes.func.isRequired,
};
