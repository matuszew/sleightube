import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { TopAppBarMainContent } from 'common/components/TopAppBar/TopAppBarMainContent/container/connect';
import { TopAppBar } from 'common/components/TopAppBar/container/connect';

import styles from './styles.scss';

export const MainLayout = ({
  children,
  className,
}) => (
  <div className={classnames(styles.MainLayout, className)}>
    <TopAppBar>
      <TopAppBarMainContent />
    </TopAppBar>
    {children}
  </div>
);

MainLayout.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

MainLayout.defaultProps = {
  className: '',
};
