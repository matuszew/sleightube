import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { TopAppBarGuestContent } from 'common/components/TopAppBar/TopAppBarGuestContent/TopAppBarGuestContent';
import { TopAppBar } from 'common/components/TopAppBar/container/connect';

import styles from './styles.scss';

export const GuestLayout = ({
  children,
  className,
}) => (
  <div className={classnames(styles.GuestLayout, className)}>
    <TopAppBar showAuthSection={false}>
      <TopAppBarGuestContent />
    </TopAppBar>
    {children}
  </div>
);

GuestLayout.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

GuestLayout.defaultProps = {
  className: '',
};
