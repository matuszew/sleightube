import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './styles.scss';

export const Sidebar = ({
  children,
  className,
}) => (
  <div className={classNames(styles.Sidebar, className)}>
    {children}
  </div>
);

Sidebar.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Sidebar.defaultProps = {
  className: '',
};
