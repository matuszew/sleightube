import React, { Component, Fragment } from 'react';
import Proptypes from 'prop-types';
import classnames from 'classnames';
import { debounce } from 'lodash';
import {
  fieldInputPropTypes,
  fieldMetaPropTypes,
} from 'redux-form';

import styles from './styles.scss';

export const required = value => (value ? undefined : 'Required');

export class TextInput extends Component {
  constructor(props) {
    super(props);

    const {
      debounceTime,
    } = this.props;

    this.handleOnChange = debounceTime
      ? debounce(this.handleOnChange, debounceTime)
      : this.handleOnChange;
  }

  state = {
    value: '',
  };

  handleChange = (event) => {
    const {
      target: {
        value,
      },
    } = event;

    this.setState({ value });
    this.handleOnChange(value);
  };

  handleOnChange = (value) => {
    const {
      input: {
        onChange,
      },
    } = this.props;

    onChange(value);
  };

  render() {
    const {
      className,
      id,
      input,
      meta: {
        error,
        touched,
      },
      placeholder,
      type,
    } = this.props;

    const {
      value,
    } = this.state;

    const hasError = error && touched;
    const inputClasses = classnames(
      styles.TextInput,
      className,
      {
        [styles.TextInput__error]: hasError,
      },
    );

    return (
      <Fragment>
        <input
          autoComplete="off"
          className={inputClasses}
          id={id}
          {...input}
          onChange={this.handleChange}
          placeholder={placeholder}
          type={type}
          value={value}
        />
        { hasError && <span className={styles.TextInput_ValidationMessage}>{error}</span> }
      </Fragment>
    );
  }
}

TextInput.propTypes = {
  className: Proptypes.string,
  debounceTime: Proptypes.number,
  id: Proptypes.string,
  input: Proptypes.shape(fieldInputPropTypes).isRequired,
  meta: Proptypes.shape(fieldMetaPropTypes).isRequired,
  placeholder: Proptypes.string,
  type: Proptypes.string,
};

TextInput.defaultProps = {
  className: '',
  debounceTime: null,
  id: Proptypes.string,
  placeholder: '',
  type: 'text',
};
