import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import mainStyles from 'common/styles/main.scss';
import styles from './styles.scss';

export const TextInputWrapper = ({
  children,
  className,
}) => (
  <div className={classnames(
    mainStyles.mdcTextField,
    styles.TextInputWrapper,
    className,
  )}
  >
    {children}
  </div>
);

TextInputWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

TextInputWrapper.defaultProps = {
  className: '',
};
