import React from 'react';
import PropTypes from 'prop-types';

export const Button = ({
  children,
  className,
  id,
  onClick,
}) => (
  <button
    className={className}
    id={id}
    onClick={onClick}
  >
    {children}
  </button>
);

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  id: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  className: '',
  id: '',
  onClick: () => {},
};
