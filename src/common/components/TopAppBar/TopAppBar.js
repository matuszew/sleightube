import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { TopAppBarSection } from 'common/components/TopAppBar/TopAppBarSection/TopAppBarSection';
import { Button } from 'common/components/Form/Button/Button';

import { AUTHENTICATION_SCOPE, API_KEY } from 'common/utils/GoogleApiLoader';

import { User } from 'common/consts/schema';

import mainStyles from 'common/styles/main.scss';
import styles from './styles.scss';

const topAppBarLogoutButtonClasses = classnames(
  'material-icons',
  mainStyles.mdcTopAppBar__actionItem,
  styles.TopAppBar_Icon,
);

const topAppBarLoginButtonClasses = classnames(
  'material-icons',
  mainStyles.mdcButton,
  mainStyles.mdcTopAppBar__actionItem,
  styles.TopAppBar_Button,
);

export class TopAppBar extends Component {
  componentDidMount() {
    const GoogleAuth = window.gapi.auth2.getAuthInstance();
    GoogleAuth.isSignedIn.listen(this.updateAuthenticationStatus);
    GoogleAuth.currentUser.get();
    this.setAuthenticationStatus();
  }

  setAuthenticationStatus = () => {
    const {
      setUserAuthorized,
      setUserUnauthorized,
    } = this.props;

    const user = window.gapi.auth2.getAuthInstance().currentUser.get();
    const isAuthorized = user.hasGrantedScopes(AUTHENTICATION_SCOPE);

    if (isAuthorized) {
      setUserAuthorized(user.getBasicProfile());
    } else {
      setUserUnauthorized();
    }
  };

  updateAuthenticationStatus = () => {
    this.setAuthenticationStatus();
  };

  signIn = () => {
    window.gapi.auth2.getAuthInstance().signIn();
  };

  signOut = () => {
    this.props.redirectToLoginPage();
    window.gapi.auth2.getAuthInstance().signOut();
  };

  render() {
    const {
      children,
      className,
      isAuthenticated,
      showAuthSection,
      userProfile,
    } = this.props;

    return (
      <header
        className={classnames(
          styles.TopAppBar,
          mainStyles.mdcTopAppBar,
          mainStyles.mdcTopAppBarFixed,
          mainStyles.mdcTopAppBarFixedScrolled,
          className,
        )}
        id="top-app-bar"
      >
        <nav className={mainStyles.mdcTopAppBar__row}>
          {children}
          { showAuthSection && (
            <TopAppBarSection className={mainStyles.mdcTopAppBar__sectionAlignEnd}>
              { !isAuthenticated && (
                <Button
                  className={topAppBarLoginButtonClasses}
                  onClick={this.signIn}
                >
                  Authenticate
                </Button>
              ) }

              { isAuthenticated && (
                <img
                  alt="avatar"
                  className={styles.TopAppBar_Avatar}
                  src={`${userProfile.avatarUrl}?key=${API_KEY}`}
                />
              )}

              <Button
                className={topAppBarLogoutButtonClasses}
                id="top-appbar-logout-action-button"
                onClick={this.signOut}
              >
                exit_to_app
              </Button>
            </TopAppBarSection>
          )}
        </nav>
      </header>
    );
  }
}

TopAppBar.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  isAuthenticated: PropTypes.bool.isRequired,
  redirectToLoginPage: PropTypes.func.isRequired,
  setUserAuthorized: PropTypes.func.isRequired,
  setUserUnauthorized: PropTypes.func.isRequired,
  showAuthSection: PropTypes.bool,
  userProfile: PropTypes.shape(User),
};

TopAppBar.defaultProps = {
  className: '',
  userProfile: null,
  showAuthSection: true,
};
