import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { TopAppBarSection } from 'common/components/TopAppBar/TopAppBarSection/TopAppBarSection';
import { TextSpan } from 'common/components/Typography/TextSpan/TextSpan';

import mainStyles from 'common/styles/main.scss';

import styles from './styles.scss';

export const TopAppBarGuestContent = ({
  className,
}) => (
  <TopAppBarSection className={
    classnames(
      mainStyles.mdcTopAppBar__sectionAlignStart,
      styles.TopApBarGuest_Content,
      className,
    )}
  >
    <TextSpan className={classnames(
      mainStyles.mdcTopAppBar__title,
      styles.TopApBarGuest_Title,
    )}
    >
      SleighTube
    </TextSpan>
  </TopAppBarSection>
);

TopAppBarGuestContent.propTypes = {
  className: PropTypes.string,
};

TopAppBarGuestContent.defaultProps = {
  className: '',
};
