import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import mainStyles from 'common/styles/main.scss';

export const TopAppBarSection = ({
  children,
  className,
}) => (
  <section className={classnames(mainStyles.mdcTopAppBar__section, className)}>
    {children}
  </section>
);

TopAppBarSection.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

TopAppBarSection.defaultProps = {
  className: '',
};
