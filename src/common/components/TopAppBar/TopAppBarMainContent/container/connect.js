import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { isDataFetching } from 'common/store/repository/selectors';
import { NAMESPACE } from 'common/consts/namespaces';
import { required } from 'common/components/Form/TextInput/TextInput';

import {
  TopAppBarMainContent as TopAppBarMainContentView,
} from 'common/components/TopAppBar/TopAppBarMainContent/TopAppBarMainContent';

import { queryYoutubeForVideos } from './actions';

const mapStateToProps = state => ({
  isSearchResultsLoading: isDataFetching(NAMESPACE.SEARCH)(state),
});

const mapDispatchToProps = dispatch => ({
  searchYoutubeForVideos: keyword => dispatch(queryYoutubeForVideos(keyword)),
});

const topAppBarMainContentView = reduxForm({
  form: NAMESPACE.SEARCH,
  onChange: (value, dispatch, props) => {
    setTimeout(() => props.submit(), 0);
  },
  onSubmit: (values, dispatch, props) => {
    props.searchYoutubeForVideos(values.search);
  },
  validate: ({ search }) => {
    const searchValidation = {
      search: required(search),
    };
    return {
      ...searchValidation,
    };
  },
})(TopAppBarMainContentView);

export const TopAppBarMainContent = connect(mapStateToProps, mapDispatchToProps)(topAppBarMainContentView);
