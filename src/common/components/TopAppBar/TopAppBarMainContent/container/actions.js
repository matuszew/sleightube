import { fetchInit } from 'common/store/repository/actions';
import { ENDPOINT } from 'common/consts/endpoints';
import { NAMESPACE } from 'common/consts/namespaces';

export const queryYoutubeForVideos = query => fetchInit(
  ENDPOINT.SEARCH,
  NAMESPACE.SEARCH,
  {
    query: {
      key: process.env.REACT_APP_YOUTUBE_API_KEY,
      maxResults: 50,
      part: 'snippet',
      type: 'video',
      q: query,
    },
  },
);
