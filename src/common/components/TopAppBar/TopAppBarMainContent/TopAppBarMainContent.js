import { Field } from 'redux-form';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { TextInput } from 'common/components/Form/TextInput/TextInput';
import { TextInputWrapper } from 'common/components/Form/TextInputWrapper/TextInputWrapper';
import { TopAppBarSection } from 'common/components/TopAppBar/TopAppBarSection/TopAppBarSection';
import { TextSpan } from 'common/components/Typography/TextSpan/TextSpan';
import { Spinner } from 'common/components/Indicator/Spinner/Spinner';

import mainStyles from 'common/styles/main.scss';
import styles from './styles.scss';

const topAppBarTitleClasses = classnames(
  mainStyles.mdcTopAppBar__title,
  styles.TopAppBarMainContent_Title,
);

const DEBOUNCE_TIME = 500;

export const TopAppBarMainContent = ({
  isSearchResultsLoading,
}) => (
  <TopAppBarSection className={mainStyles.mdcTopAppBar__sectionAlignStart}>
    <TextSpan className={topAppBarTitleClasses}>
      SleighTube
    </TextSpan>
    <TextInputWrapper className={styles.TopAppBarMainContent_InputWrapper}>
      <Field
        component={TextInput}
        debounceTime={DEBOUNCE_TIME}
        id="top-app-bar-search-input"
        name="search"
        placeholder="Search"
        type="text"
      />
      { isSearchResultsLoading
      && <Spinner className={styles.TopAppBarMainContent_InputSpinner} />
      }
    </TextInputWrapper>
  </TopAppBarSection>
);

TopAppBarMainContent.propTypes = {
  isSearchResultsLoading: PropTypes.bool.isRequired,
};
