import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { APP_PATHS } from 'common/routing/path';

import {
  saveUserAuthorized,
  saveUserUnauthorized,
} from 'common/store/auth/actions';

import {
  isUserAuthenticated,
  getUserProfile,
} from 'common/store/auth/selectors';
import { TopAppBar as TopAppBarView } from 'common/components/TopAppBar/TopAppBar';

const mapStateToProps = state => ({
  isAuthenticated: isUserAuthenticated(state),
  userProfile: getUserProfile(state),
});

const mapDispatchToProps = dispatch => ({
  setUserAuthorized: profile => dispatch(saveUserAuthorized(profile)),
  setUserUnauthorized: () => {
    dispatch(saveUserUnauthorized());
  },
  redirectToLoginPage: () => {
    dispatch(push(APP_PATHS.LOGIN));
  },
});

export const TopAppBar = connect(mapStateToProps, mapDispatchToProps)(TopAppBarView);
