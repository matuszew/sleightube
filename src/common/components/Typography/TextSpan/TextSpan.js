import React from 'react';
import PropTypes from 'prop-types';

export const TextSpan = ({
  children,
  className,
}) => (
  <span className={className}>
    {children}
  </span>
);

TextSpan.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

TextSpan.defaultProps = {
  className: '',
};
