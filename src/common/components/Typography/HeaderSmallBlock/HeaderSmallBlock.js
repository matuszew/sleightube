import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const HeaderSmallBlock = ({
  children,
  className,
}) => (
  <h3 className={classnames(styles.HeaderSmallBlock, className)}>
    {children}
  </h3>
);

HeaderSmallBlock.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

HeaderSmallBlock.defaultProps = {
  className: '',
};
