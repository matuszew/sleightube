import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const HeaderLargeBlock = ({
  children,
  className,
}) => (
  <h2 className={classnames(styles.HeaderLargeBlock, className)}>
    {children}
  </h2>
);

HeaderLargeBlock.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

HeaderLargeBlock.defaultProps = {
  className: '',
};
