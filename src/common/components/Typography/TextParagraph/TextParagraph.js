import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const TextParagraph = ({
  children,
  className,
}) => (
  <p className={classnames(styles.TextParagraph, className)}>
    {children}
  </p>
);

TextParagraph.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

TextParagraph.defaultProps = {
  className: '',
};
