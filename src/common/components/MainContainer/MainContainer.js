import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const MainContainer = ({
  children,
  className,
  fullWidth,
}) => (
  <div className={classnames(
    styles.MainContainer,
    { [styles.MainContainer__fullWidth]: fullWidth },
    className,
    )}
  >
    {children}
  </div>
);

MainContainer.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  fullWidth: PropTypes.bool,
};

MainContainer.defaultProps = {
  className: '',
  fullWidth: false,
};
