import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const Spinner = ({
  className,
}) => (
  <div className={classnames(styles.Spinner, className)}>
    <div className={styles.DoubleBounce__1} />
    <div className={styles.DoubleBounce__2} />
  </div>
);

Spinner.propTypes = {
  className: PropTypes.string,
};

Spinner.defaultProps = {
  className: '',
};
