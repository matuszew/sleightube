import React from 'react';

export const wrapRoutesWithComponent = (Component, routesToWrap) =>
  routesToWrap.map(route => ({
    ...route,
    component: () => (
      <Component>
        <route.component />
      </Component>
    ),
  }));
