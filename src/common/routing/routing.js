import { wrapRoutesWithComponent } from 'common/routing/utils';
import { APP_PATHS } from 'common/routing/path';

import { Homepage } from 'Homepage/Homepage';
import { LogIn } from 'LogIn/container/connect';


import { MainLayout } from 'Layout/MainLayout/MainLayout';
import { GuestLayout } from 'Layout/GuestLayout/GuestLayout';

const MAIN_LAYOUT_ROUTES = [
  {
    exact: true,
    path: APP_PATHS.HOMEPAGE,
    component: Homepage,
  },
];

const GUEST_ROUTES = [
  {
    exact: true,
    path: APP_PATHS.LOGIN,
    component: LogIn,
  },
];

export const ROUTER_CONFIG = [
  ...wrapRoutesWithComponent(MainLayout, MAIN_LAYOUT_ROUTES),
  ...wrapRoutesWithComponent(GuestLayout, GUEST_ROUTES),
];
