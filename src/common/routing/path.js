const LOGIN = '/';
const HOMEPAGE = '/movies';

export const APP_PATHS = {
  LOGIN,
  HOMEPAGE,
};
