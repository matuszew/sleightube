export const AUTHENTICATION_SCOPE = process.env.REACT_APP_YOUTUBE_SCOPE;
export const API_KEY = process.env.REACT_APP_YOUTUBE_API_KEY;

export const GoogleApiLoader = () => {
  const discoveryUrl = 'https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest';
  return window.gapi.client.init({
    apiKey: process.env.REACT_APP_YOUTUBE_API_KEY,
    clientId: process.env.REACT_APP_YOUTUBE_CLIENT_ID,
    discoveryDocs: [discoveryUrl],
    scope: AUTHENTICATION_SCOPE,
  });
};
