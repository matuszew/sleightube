import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { reducer as repositoryReducer } from 'common/store/repository/reducers';
import { reducer as authenticationReducer } from 'common/store/auth/reducer';

const repository = repositoryReducer([]);

const appReducer = combineReducers({
  repository,
  form: formReducer,
  auth: authenticationReducer,
});

export const rootReducer = (state, action) => appReducer(state, action);
