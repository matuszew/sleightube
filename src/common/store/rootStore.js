import { applyMiddleware, createStore } from 'redux';

import { composeWithDevTools } from 'redux-devtools-extension'; // eslint-disable-line
import { routerMiddleware } from 'react-router-redux';

import { rootReducer } from './rootReducer';
import { createRootEpic } from './rootEpic';

export const createRootStore = (initialState = {}, history) => {
  const epicMiddleware = createRootEpic();

  return createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(
      epicMiddleware,
      routerMiddleware(history),
    )),
  );
};

