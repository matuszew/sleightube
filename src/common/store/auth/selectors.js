import { createSelector } from 'reselect';
import get from 'lodash/get';

import { NAMESPACE } from 'common/consts/namespaces';

export const isUserAuthenticated = createSelector(
  state => get(state, NAMESPACE.AUTH),
  auth => auth.isAuthenticated,
);

export const getUserProfile = createSelector(
  state => get(state, NAMESPACE.AUTH),
  (auth) => {
    if (auth?.user) {
      return {
        avatarUrl: auth.user.Paa,
      };
    }
    return null;
  },
);
