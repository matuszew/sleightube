export const AUTH_AUTHORIZED = 'AUTH_AUTHORIZED';
export const AUTH_UNAUTHORIZED = 'AUTH_UNAUTHORIZED';

export const saveUserAuthorized = payload => ({ type: AUTH_AUTHORIZED, payload });
export const saveUserUnauthorized = () => ({ type: AUTH_UNAUTHORIZED });
