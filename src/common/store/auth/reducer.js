import { AUTH_AUTHORIZED, AUTH_UNAUTHORIZED } from './actions';

const initialState = {
  isAuthenticated: false,
  user: null,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_AUTHORIZED:
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload,
      };
    case AUTH_UNAUTHORIZED:
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };
    default:
      return state;
  }
};
