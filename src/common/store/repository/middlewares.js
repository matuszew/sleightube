import { combineEpics, ofType } from 'redux-observable';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { ajax as ajaxObservable } from 'rxjs/observable/dom/ajax';
import queryString from 'query-string';

import {
  FETCH_INIT,
  fetchFailed,
  fetchSuccess,
} from './actions';


export const requestFromAction = (action$, sources) => ({
  namespace,
  params,
  payload,
  type,
  url,
}) => {
  const responseToAction = ({ response }) => (
    fetchSuccess(url, namespace, params, response)
  );

  const handleError = ({ response }) => of(fetchFailed(url, namespace, params, response));

  return ajaxObservable({
    url: `${url}${queryString.stringify(params.query)}`,
  }).pipe(
    map(responseToAction),
    catchError(handleError),
  );
};

export const fetchDataEpic = (action$, store, deps) => action$.pipe(
  ofType(FETCH_INIT),
  mergeMap(requestFromAction(action$)),
);


export const repositoryEpics = combineEpics(fetchDataEpic);
