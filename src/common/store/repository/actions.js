export const FETCH_INIT = 'FETCH_INIT';
export const FETCH_SUCCESS = 'FETCH_SUCCESS';
export const FETCH_FAILED = 'FETCH_FAILED';

export const createRepositoryAction = type => (
  url,
  namespace,
  params,
  payload,
) => ({
  type,
  url,
  namespace,
  payload,
  params,
});

export const fetchInit = createRepositoryAction(FETCH_INIT);
export const fetchSuccess = createRepositoryAction(FETCH_SUCCESS);
export const fetchFailed = createRepositoryAction(FETCH_FAILED);
