import { createEpicMiddleware, combineEpics } from 'redux-observable';

import { repositoryEpics } from 'common/store/repository/middlewares';

const applicationEpics = combineEpics(repositoryEpics);

export const createRootEpic = () => createEpicMiddleware(applicationEpics);
