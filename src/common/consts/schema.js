import PropTypes from 'prop-types';

export const User = {
  avatarUrl: PropTypes.string.isRequired,
};
